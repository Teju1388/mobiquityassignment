package com.cafetownsend.testclass;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.cafetownsend.base.TestBase;
import com.cafetownsend.pages.HomePage;
import com.cafetownsend.pages.LoginPage;
import com.cafetownsend.util.ExcelUtility;

public class HomePageTest extends TestBase {
	LoginPage loginDtls;
	HomePage homePage;

	public HomePageTest() {
		super();
	}

	@BeforeClass
	public void setUp() throws Exception {
		initialization();
		Reporter.log("Welcome to Home Page Test Cases");
		loginDtls = new LoginPage();
		loginDtls.enterUserName(prop.getProperty("username"));
		loginDtls.enterUserpassword(prop.getProperty("password"));
		homePage = loginDtls.clickLogInBtn();
	}

	// ** Validate the Login has landed on Home Page Correctly by verifying the
	// welcome message *//
	@Test
	public void verifyHomePageWelcomeMsg() {
		String homeTitle = homePage.validateHomePageTitle();
		Assert.assertEquals(homeTitle, "Hello Luke");
		Reporter.log("Successfully Verified Home Page Welcome Message");
	}

	// ** Read the data from excel and create New Employee Data **//
	@DataProvider(name = "createemployeeDetails")
	public Object[][] dataProvidercreate() {
		Object[][] testdata = ExcelUtility.getTestData("CreateEmployeeDetails");
		return testdata;
	}

	// ** Get the total number of employees in the list **//
	@Test(dependsOnMethods = { "verifyHomePageWelcomeMsg" })
	public void empCountBeforeAdd() throws Exception {
		bfrAddCount = homePage.checkNoOfEmployees();
		Reporter.log("No. Of Employees Before Adding : " + bfrAddCount);
	}

	// ** Add Employees to List **//
	@Test(dependsOnMethods = { "empCountBeforeAdd" }, dataProvider = "createemployeeDetails")
	public void createEmployees(String frstName, String lstName, String strtDate, String email) {
		homePage.clickOncreateEmployee();
		homePage.enterEmployeeDetails(frstName, lstName, strtDate, email);
		homePage.clickOnAdd();
		addNum = addNum + 1;
	}

	// ** Verify if the employees are added correctly into the employee list by
	// verifying the count of employees **//
	@Test(dependsOnMethods = { "createEmployees" })
	public void validateEmployeeAddition() throws Exception {
		aftAddCount = homePage.checkNoOfEmployees();
		Reporter.log("Count of Employees after adding : " + aftAddCount);
		Assert.assertEquals(aftAddCount, bfrAddCount + addNum);
		Reporter.log("Successfully Added employees retrived from Excel testdata sheet");
	}

	// ** To create an employee with invalid date. Read the data from excel **//
	@DataProvider(name = "invalidDateCreation")
	public Object[][] dataProviderinvalidDate() {
		Object[][] testdata = ExcelUtility.getTestData("InvalideDateCreation");
		return testdata; 
	}
    
	//** Validate if the date entered is incorrect **//
	@Test(dependsOnMethods = { "validateEmployeeAddition" }, dataProvider = "invalidDateCreation")
	public void createEmplsinvalidDt(String frstName, String lstName, String strtDate, String email) {
		homePage.clickOncreateEmployee();
		homePage.enterEmployeeDetails(frstName, lstName, strtDate, email);
		homePage.clickOnAdd();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		if (wait.until(ExpectedConditions.alertIsPresent()) != null) {
			String alertMsg = driver.switchTo().alert().getText();
			String expectedAlert = "Error trying to create a new employee: {\"start_date\":[\"can't be blank\"]})";
			driver.switchTo().alert().accept();
			Assert.assertEquals(alertMsg,expectedAlert);
			Reporter.log("Invalid Date has been entered");
			homePage.clickOnCancel();
		}
	}

	// ** Read the data to edit the created employee details in the list **//
	@DataProvider(name = "editemployeeDetails")
	public Object[][] dataProvideredit() {
		Object[][] testdata = ExcelUtility.getTestData("EditEmployeeDetails");
		return testdata;
	}

	// ** Perform editing of a created employee in the list **//
	@Test(dependsOnMethods = { "createEmplsinvalidDt" }, dataProvider = "editemployeeDetails")
	public void employeeEdit(String FName, String LName, String strtDate, String email) {
		homePage.goToEmployeeDetails(FName, LName);
		homePage.clickOnEdit();
		homePage.clearDataAndEdit(strtDate, email);
		homePage.clickOnUpdate();
		Reporter.log("Edited the Employee Data Successfully");
	}

	@Test(dependsOnMethods = { "employeeEdit" }, dataProvider = "editemployeeDetails")
	public void doubleClickViewEmp(String fName, String lName, String strtDate, String email) {
		homePage.doubleClickEle(fName, lName);
		homePage.clickOnBack();
		Reporter.log("Double click on edited Employee to view Details");
	}

	@Test(dependsOnMethods = { "doubleClickViewEmp" })
	public void empCountBeforeDelete() throws Exception {
		bfrDelCount = homePage.checkNoOfEmployees();
		Reporter.log("No. Of Employees Before Deleting : " + bfrDelCount);
	}

	// ** Perform deletion of created employee in the list **//
	@Test(dependsOnMethods = { "doubleClickViewEmp" }, dataProvider = "editemployeeDetails")
	public void employeeDelete(String FName, String LName, String strtDate, String email) throws Exception {
		homePage.goToEmployeeDetails(FName, LName);
		homePage.clickOnDel();
		driver.switchTo().alert().accept();
		subNum = subNum + 1;
	}

	// ** Verify if the employee data is deleted from the employeeList **//
	@Test(dependsOnMethods = { "employeeDelete" })
	public void validateEmployeeDeletion() throws Exception {
		homePage.logOut();
		loginDtls.enterUserName(prop.getProperty("username"));
		loginDtls.enterUserpassword(prop.getProperty("password"));
		homePage = loginDtls.clickLogInBtn();
		aftDelCount = homePage.checkNoOfEmployees();
		Reporter.log("Count of Employees after Deleting : " + aftDelCount);
		Assert.assertEquals(aftDelCount, bfrDelCount - subNum);
		Reporter.log("Successfully Deleted employees");
	}

	@AfterClass
	public void tearDown() {
		homePage.logOut();
		String logOutValidate = loginDtls.validateLoginPageTitle();
		Assert.assertEquals(logOutValidate, "CafeTownsend-AngularJS-Rails");
		Reporter.log("Application successfully Logged Out");
		driver.quit();
	}

}
