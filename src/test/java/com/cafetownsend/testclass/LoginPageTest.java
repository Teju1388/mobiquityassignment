package com.cafetownsend.testclass;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.cafetownsend.base.TestBase;
import com.cafetownsend.pages.HomePage;
import com.cafetownsend.pages.LoginPage;

public class LoginPageTest extends TestBase {
	LoginPage loginPage;
	HomePage homePage;
	
	public LoginPageTest() {
		super();
	}
	
	@BeforeMethod
	public void setUp() throws Exception {
		initialization();
		Reporter.log("Welcome to Login Page Test Cases");
		loginPage = new LoginPage();
	}
	
	// ** Validate if the URL launched is on the Login Page **//
	@Test
	public void validateLoginTitle() {
		String loginTitle = loginPage.validateLoginPageTitle();
		Assert.assertEquals(loginTitle, "CafeTownsend-AngularJS-Rails");	
	}
	
	// ** Login with valid credentials **//
	@Test(dependsOnMethods= {"validateLoginTitle"})
	public void aloginCredential(){
		loginPage.enterUserName(prop.getProperty("username"));
		loginPage.enterUserpassword(prop.getProperty("password"));
		homePage = loginPage.clickLogInBtn();
	}
	
	// ** Login with invalid credentials and verify if the "Invalid Login" message is displayed **//
	@Test(dependsOnMethods = {"validateLoginTitle"})
	public void invalidLogin() {
		loginPage.enterUserName(prop.getProperty("username"));
		loginPage.enterUserpassword(prop.getProperty("invalidPwd"));
		homePage = loginPage.clickLogInBtn();
		String invalidLogin = loginPage.getinvalidLogintext();
		Assert.assertEquals(invalidLogin, "Invalid username or password!");
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}
