package com.cafetownsend.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cafetownsend.base.TestBase;

//** Object Repository for Home Page WebElements **//
public class HomePage extends TestBase {
	WebDriverWait wait = new WebDriverWait(driver, 30);

	@FindBy(id = "bAdd")
	WebElement createBtn;

	@FindBy(xpath = "//div[@class='formFooter']/button[contains(text(),'Add')]")
	WebElement addBtn;

	@FindBy(xpath = "//div[@class='formFooter']/button[contains(text(),'Update')]")
	WebElement updBtn;

	@FindBy(id = "bEdit")
	WebElement editBtn;

	@FindBy(id = "bDelete")
	WebElement delBtn;

	@FindBy(xpath = "//ul[@id='sub-nav']/li/a[contains(text(),'Back')]")
	WebElement backBtn;
	
	@FindBy(xpath = "//ul[@id='sub-nav']/li/a[contains(text(),'Cancel')]")
	WebElement cancelBtn;

	@com.paulhammant.ngwebdriver.ByAngularModel.FindBy(model = "selectedEmployee.firstName")
	WebElement frstName;

	@com.paulhammant.ngwebdriver.ByAngularModel.FindBy(model = "selectedEmployee.lastName")
	WebElement lstName;

	@com.paulhammant.ngwebdriver.ByAngularModel.FindBy(model = "selectedEmployee.startDate")
	WebElement strtDt;

	@com.paulhammant.ngwebdriver.ByAngularModel.FindBy(model = "selectedEmployee.email")
	WebElement email;

	@FindBy(xpath = "//div[@id='employee-list-container']/ul/li")
	List<WebElement> employeeList;

	@FindBy(id = "greetings")
	WebElement wlcmMsg;

	@FindBy(xpath = "//p[contains(text(),'Logout')]")
	WebElement logOut;

	public HomePage() {
		PageFactory.initElements(driver, this);
	}

	public String validateHomePageTitle() {
		return wlcmMsg.getText();
	}

	public void clickOncreateEmployee() {
		createBtn.click();
	}

	public void enterEmployeeDetails(String firstName, String lastName, String startDate, String emailId) {
		

		wait.until(ExpectedConditions.elementToBeClickable(frstName)).sendKeys(firstName);

		lstName.sendKeys(lastName);

		strtDt.sendKeys(startDate);

		email.sendKeys(emailId);
		
	}

	public void goToEmployeeDetails(String firstName, String lastName) {
		WebElement emplToEdit = driver
				.findElement(By.xpath("//div[@id='employee-list-container']/ul/li[contains(text(), '" + firstName + " "
						+ lastName + "')][1]"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", emplToEdit);
		emplToEdit.click();
	}

	public void doubleClickEle(String fName, String lName) {
		WebElement emplToView = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
				"//div[@id='employee-list-container']/ul/li[contains(text(), '" + fName + " " + lName + "')][1]")));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", emplToView);
		Actions action = new Actions(driver);
		action.moveToElement(emplToView).doubleClick().build().perform();
	}

	public void clearDataAndEdit(String startdt, String emailId) {

		strtDt.clear();
		strtDt.sendKeys(startdt);
		email.clear();
		email.sendKeys(emailId);
	}

	public int checkNoOfEmployees() throws InterruptedException {
		return counter = employeeList.size();
	}

	public void clickOnAdd() {
		addBtn.click();
	}

	public void clickOnEdit() {
		editBtn.click();
	}

	public void clickOnUpdate() {
		updBtn.click();
	}

	public void clickOnDel() {
		delBtn.click();
	}

	public void clickOnBack() {
		backBtn.click();
	}
	
	public void clickOnCancel() {
		cancelBtn.click();
	}

	public void logOut() {
		logOut.click();
	}

}
