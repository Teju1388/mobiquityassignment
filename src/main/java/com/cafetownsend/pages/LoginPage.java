package com.cafetownsend.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cafetownsend.base.TestBase;

//** Object Repository for Login Page WebElements **//
public class LoginPage extends TestBase {
	WebDriverWait wait = new WebDriverWait(driver, 30);

	@com.paulhammant.ngwebdriver.ByAngularModel.FindBy(model = "user.name")
	WebElement userName;

	@com.paulhammant.ngwebdriver.ByAngularModel.FindBy(model = "user.password")
	WebElement userPassword;

	@FindBy(xpath = "//button[@type='submit']")
	WebElement loginBtn;

	@FindBy(xpath = "//p[contains(text(),'Invalid username or password!')]")
	WebElement invalidLogin;

	public LoginPage() {
		PageFactory.initElements(driver, this);
	}

	public String validateLoginPageTitle() {
		return driver.getTitle();
	}

	public void enterUserName(String userId) {
		wait.until(ExpectedConditions.elementToBeClickable(userName)).clear();
		userName.sendKeys(userId);
	}

	public void enterUserpassword(String userPwd) {
		userPassword.clear();
		userPassword.sendKeys(userPwd);
	}

	public String getinvalidLogintext() {
		return invalidLogin.getText();
	}

	public HomePage clickLogInBtn() {
		loginBtn.click();
		return new HomePage();
	}

}
