package com.cafetownsend.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.cafetownsend.util.ExcelUtility;
import com.paulhammant.ngwebdriver.NgWebDriver;

public class TestBase {
	public static WebDriver driver;
	public static NgWebDriver ngdriver = new NgWebDriver((JavascriptExecutor) driver);
	public static Properties prop;
	public int counter;
	public int bfrAddCount;
	public int aftAddCount;
	public int bfrDelCount;
	public int aftDelCount;
	public int addNum;
	public int subNum;

	public TestBase() {
		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream(
					"D:\\Selenium\\MobiquityAssignment\\src\\main\\java\\com\\cafetownsend\\config\\config.properties");
			prop.load(ip);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void initialization() throws Exception {
		String browserName = prop.getProperty("browser");
		if (browserName.equals("chrome")) {
			driver = new ChromeDriver();
		} else if (browserName.equals("firefox")) {
			driver = new FirefoxDriver();
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get(prop.getProperty("url"));
		ExcelUtility.setExcelFile(prop.getProperty("xcelpath"), "EmployeeDetails");
	}

}
